# a=10
# print(type(a))             # will return INT 

# a=10,                      # comma makes it TUPLE
# print(type(a))             # will return TUPLE

a=10,30,69,100
b=('a','b','c')

# Length
length=len(a)

# Max Value
m=max(b)

# Min Value
mn=min(b)

# Concatenation
conc=a+b

# Repeatition
rp=a*2                      # repeats or prepares copies

# print(length)
# print(m)
# print(mn)
# print(conc)
# print(rp)

# Membership
chk=30 in a
# print(chk)                  # returns TRUE or FALSE; checks if element is present in tuple or not
ch2=30  not in a
# print(ch2)

# Index
ind=a.index(10)
print(ind)