ls=['red','green','blue']
st=int('103')
n=10
# Convert to String
convert_to_string=str(n)
# print(type(convert_to_string))

# Convert to Boolean
b=bool(n)
# print(b)

# Convert to float
f=float(n)
# print(f)

# Convert to List
# l=list(n)             numbers can't be converted to list because we can't index numbers; but string
ab='String'
# print(list(ab))

# Convert to Tuple
t=tuple(ab)             # can't be converted to directory because there are no key:value pairs
print(t)