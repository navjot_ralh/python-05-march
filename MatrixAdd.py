a=[[1,2,3],[4,5,6],[7,8,9]]
b=[[10,11,12],[13,14,15],[16,17,18]]
c=[[0,0,0],[0,0,0],[0,0,0]]
for i in range(len(a)):
    for j in range(len(a[0])):
        c[i][j]=a[i][j]+b[i][j]
print(c)

a=[]
ar1=[x for x in range(10,40,10)]
a.append(ar1)
ar2=[x for x in range(40,70,10)]
a.append(ar2)
ar3=[x for x in range(70,100,10)]
a.append(ar3)
b=[[100,110,120],[130,140,150],[160,170,180]]
# for reading indices inside list
# ls=[]
# for i in range(len(a)):
#     c=[]
#     for j in range(len(ar1)):
#         c.append(0)
#     ls.append(c)
# print(ls)

result=[[a[i][j]+b[i][j] for j in range(len(a[0]))] for i in range(len(a))]
print(result)