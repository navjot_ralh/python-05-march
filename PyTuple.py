# ls=['red','green','blue','black']
# ls[1]='yellow'                # possible; because list allows alterations
# print(ls)

# t=tuple(ls)
# t[1]='green'                # will produce error: 'TUPLE' OBJECT DOESN'T SUPPORT ITEM ASSIGNMENT; so is used as a secure method to define a list
# print(t)

# c=ls.count('red')
# print(c)

# Creating a tuple
tp=(10,20,30,40,50,2,3,4,5,6,34,34,56)
print(tp[0])                # allows access to individual elements
print(tp[-1])
print(tp[1:])
print(tp[:-1])
print(tp[::2])              # step-size
print(tp[::3])